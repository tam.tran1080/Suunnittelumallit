package Obssrver;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

public class ClockTimer extends Observable {
    
        private int sec  = 0; // 60
        private int min  = 0; // 60
        private int hour = 0; // 24
        
        public int getSec(){
            return sec;
        }
        public int getMin(){
            return min;
        }
        public int getHour(){
            return hour;
        }
    
        private Timer timer = new Timer(true);
        private TimerTask timerTask = new callTick();
        
    
    public ClockTimer(int sec, int min, int hour){
        
        this.sec  = sec;
        this.min  = min;
        this.hour = hour;
        timer.schedule(timerTask,0 , 1000);
        
    }
        
         private void tick(){
            
            sec++; // sekunttikellon aloitus
            
            if(sec >= 60){
                sec = 0; // resetoi sekunnit nolliksi kun minuutti tulee täyteen
                min++; // yksi minuutti lisää
            }
            
            if(min >= 60){
                min = 0; // resetoi minuutit nolliksi kun tunti tulee täyteen
                hour++; // yksi minuutti lisää
            }
            
            if(hour >= 24){
                hour = 0; // resetoi tunnit nolliksi kun 24h tulee täyteen
            }
            
            setChanged();
            notifyObservers();
            
        }
         
            private class callTick extends TimerTask {         
                
                @Override         
                public void run() {             
                    tick();        
                    
                }            
            }
        
    }

