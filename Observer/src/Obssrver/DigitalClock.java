package Obssrver;

import java.util.Observer; 
import java.util.Observable; 

public class DigitalClock implements Observer {    
    
    private ClockTimer subject;    
    
    public DigitalClock(ClockTimer subjectClock) {   
        
        subject = subjectClock;         
        subject.addObserver(this);     
    
    }     
    
    @Override     
    public void update(Observable o, Object arg) {   
        
        if (subject == o) {             
            formatTime();         
        
        }     
    }         
    private void formatTime() {         
        
        int sec  = subject.getSec();         
        int min  = subject.getMin();         
        int hour = subject.getHour();         
        String clock = "";   
        String ampm = "";
        String seconds = "";
        
        //clock = "" + hour + ":" + min + ":" + sec; // Toimii, mutta ei ole ideaali
        
        if (hour >= 12){ // Toimii, mutta tämäkään ei ole ideaali, näyttää 24h
            ampm += " PM"; // vaikka pitäisi näyttää 12h, että kävisi järkeen
        }
        else {
            ampm += " AM";
        }

        if (min < 10){ // Kellon rakenne
            clock += hour + ":0" + min;
        }
        else {             
            clock += hour + ":" + min;         
        }
        
        seconds += "           sekunnit: " + sec;
        
        System.out.println(clock + ampm + seconds);
          
    } 
}