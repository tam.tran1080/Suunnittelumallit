package TemplateMethod;

public class Sledge extends Operator {
    
    public Sledge(){
        health = 100;
    }

    @Override
    void mission() {
        System.out.println("Sledge");
    }

    @Override
    int primary() {
        return 45;
    }

    @Override
    int secondary() {
        return 32;
    }
}
