package TemplateMethod;

public class Mute  extends Operator {
    
    public Mute(){
        health = 100;
    }

    @Override
    void mission() {
        System.out.println("Mute");
    }

    @Override
    int primary() {
        return 29;
    }

    @Override
    int secondary() {
        return 53;
    }
    
}