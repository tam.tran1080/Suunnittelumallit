package TemplateMethod;

public abstract class Operator {
    
    abstract void mission();
    abstract int primary();
    abstract int secondary();
    protected int health;
    
    final int getHealth(){
        return health;
    }
    
    final void takeDmg(int h){
        health = health - h;
    }
    
}
