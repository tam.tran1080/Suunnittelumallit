package TemplateMethod;

public class Castle  extends Operator {
    
    public Castle(){
        health = 100;
    }

    @Override
    void mission() {
        System.out.println("Castle");
    }

    @Override
    int primary() {
        return 66;
    }

    @Override
    int secondary() {
        return 33;
    }

}