package TemplateMethod;

import java.util.Scanner;

public class SiegeGame extends Game {
    
    Scanner sc = new Scanner(System.in);
    Operator orange, blue; // Team colors 
    String winner;
    OperatorFactory of = new OperatorFactory();

    // player needs to pick operator and system will randomize opponent
    @Override
    void initializeGame() {
        System.out.println("Pick your operator: 1) Caste 2) Mute");
        orange = of.makeOperator(sc.nextInt());
        blue = of.makeOperator((int)(Math.random()*4+1));
        
                System.out.println("Orange team operator: ");         
                orange.mission();         
                System.out.println("Blue team operator: ");         
                blue.mission();
        
    }

    // Operator's damage engine
    @Override
    void makePlay(int player) {
        switch ((int) (Math.random()*1+1)){
            case(1):
                orange.takeDmg(blue.primary()*(player));
                break;
            case(2):
                orange.takeDmg(blue.secondary()*(player));
                break;
        }
        System.out.println("Your operator's health: " + orange.getHealth());
    }

    @Override
    boolean endOfGame() {
        while(orange.getHealth()>0&&blue.getHealth()>0) { // Both still have health
            return false;
        }
        if (orange.getHealth()<=0&&blue.getHealth()<=0) { // Both runs out of health
            winner = "Draw";
        }
        else if(orange.getHealth()<=0){
                        winner = "Operator " + blue.toString() + " won"; 
        }
        else winner = "Operator " + orange.toString() + " won";         
        return true;
    }

    @Override
    void printWinner() {
        System.out.println(winner);
    }
    
    
    
}
