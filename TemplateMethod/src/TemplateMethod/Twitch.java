package TemplateMethod;

public class Twitch extends Operator {
    
    public Twitch(){
        health = 100;
    }

    @Override
    void mission() {
        System.out.println("Twitch");
    }

    @Override
    int primary() {
        return 66;
    }

    @Override
    int secondary() {
        return 33;
    }
    
    
}
