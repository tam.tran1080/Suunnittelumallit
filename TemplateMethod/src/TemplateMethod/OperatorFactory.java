package TemplateMethod;

public class OperatorFactory {
    
    public Operator makeOperator(int i){
        switch (i){
            case(1):                 
                return new Castle();             
            case(2):                 
                return new Mute();             
            case(3):                 
                return new Sledge();
            case(4):                 
                return new Twitch();
        }         
        return null;     
    }
        }
    

