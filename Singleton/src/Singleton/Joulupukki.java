package Singleton;

public enum Joulupukki {
    
    INSTANCE;

  public void lahjoja(){
      
      System.out.println("Joulupukilla on lahjoja annettavana");
  }
  
  public static void main(String... aArgs){
      
    Joulupukki aattona = Joulupukki.INSTANCE;
    aattona.lahjoja();
   
  }
  
}
