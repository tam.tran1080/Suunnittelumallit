
package abstractfactory;

public class Main {
    
    public static void main(String[] args){
        
        Jasper jasper = new Jasper(new Adidas()); // Adidas() tai Boss()
        
        System.out.println(jasper);
        
    }
    
}
