package abstractfactory;
// Adidasfactory
public class Adidas implements VaateTehdas {

    public Paita CreatePaita(){
        
        return new AdidasPaita();
        
    }

    public Housut CreateHousut(){
        
        return new AdidasHousut();
        
    }

    public Takki CreateTakki(){
        
        return new AdidasTakki();
        
    }
    
}
