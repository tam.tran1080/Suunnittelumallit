package abstractfactory;
// contretefactory
public class Boss implements VaateTehdas {
    

    public Paita CreatePaita(){
        
        return new BossPaita();
        
    }

    public Housut CreateHousut(){
        
        return new BossHousut();
        
    }

    public Takki CreateTakki(){
        
        return new BossTakki();
        
    }
}
