package abstractfactory;
// AbstractFactory
public interface VaateTehdas {
    
    public abstract Paita CreatePaita();
    public abstract Housut CreateHousut();
    public abstract Takki CreateTakki();

}
