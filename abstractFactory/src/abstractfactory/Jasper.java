package abstractfactory;

public class Jasper {
    
    Paita paita = null;
    Housut housut = null;
    Takki takki = null;

public Jasper(VaateTehdas vaatteet){
    
    paita = vaatteet.CreatePaita();
    housut = vaatteet.CreateHousut();
    takki = vaatteet.CreateTakki();
    
    }

public String toString(){
    
    System.out.println("Jasperilla on päällä " + paita);
    System.out.println("Jasperilla on päällä " + housut);
    System.out.println("Jasperilla on päällä " + takki);
    
    return null;
    
    }
    
}
