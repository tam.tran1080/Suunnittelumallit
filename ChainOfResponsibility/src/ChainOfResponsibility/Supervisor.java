package ChainOfResponsibility;

public class Supervisor extends Salary {
    private double allowed = 1.02 * salary;

    public void processRequest(RaiseRequest request) {
        if (request.getSum() <= salary){
            System.out.println("Supervisor cannot lower your salary");
        }
        
        else if (request.getSum() <= allowed) {
            System.out.println();
            System.out.println("Supervisor can handle request");
            System.out.println("Your salary will be " + request.getSum());
            System.out.println();
                       
        }
        
        else if (successor != null) {
            successor.processRequest(request);
        
        }
    
    }
    
}
