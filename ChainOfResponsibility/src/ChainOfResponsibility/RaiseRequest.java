package ChainOfResponsibility;

public class RaiseRequest {
    private double sum;
    
    public RaiseRequest(double sum){
        this.sum = sum;
    }
    
    public void setSum(double sum){
        this.sum = sum;
    }
    
    public double getSum(){
        return sum;
    }
    
}
