package ChainOfResponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
public static void main(String[] args){
    Supervisor supervisor = new Supervisor(); // 1
    Executive executive = new Executive();    // 2
    President president = new President();    // 3
    
    
    supervisor.setSuccessor(executive);       // Successor
    executive.setSuccessor(president);        // Successor
    
    System.out.println("Your salary is " + Salary.salary + "\n");
    
    try {
        while(true){
            System.out.println("Your raise request?");
            double request = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
            supervisor.processRequest(new RaiseRequest(request));
        
        }
    
    }
        catch (Exception e){
            System.exit(1);
        }

    }

}

