package ChainOfResponsibility;

public class Executive extends Salary {
    private double min = 1.02 * salary;
    private double max = 1.05 * salary;

    public void processRequest(RaiseRequest request) {
        if (request.getSum() <= max && request.getSum() > min) { // lower than max, higher than min
            System.out.println();
            System.out.println("Executive can handle request");
            System.out.println("Your salary will be " + request.getSum());
            System.out.println();
        
        }
        
        else if (successor != null) {
            successor.processRequest(request);

        }
        
    }
    
}
