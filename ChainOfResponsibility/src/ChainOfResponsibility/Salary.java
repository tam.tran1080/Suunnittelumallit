package ChainOfResponsibility;

public abstract class Salary {
    protected static double salary = 2500;
    protected Salary successor;
    
    public void setSuccessor(Salary successor){
        this.successor = successor;
        
    }
    abstract public void processRequest(RaiseRequest request);
    
}
