package ChainOfResponsibility;

public class President extends Salary {
    private double allowed = 1.05 * salary;
    
    public void processRequest(RaiseRequest request) {
        if (request.getSum() > salary){
            System.out.println();
            System.out.println("President will handle request");
            System.out.println("Your salary will be " + request.getSum());
            System.out.println();
        
        }
        
        else if (successor != null) {
            successor.processRequest(request);
            System.out.println("Nobody handled your request...");
            
        }
                
    }
    
}
