package Memento;

public class Memento {
    private final int GNumber;
    
    public Memento (int number){
        this.GNumber = number;
    }
    
    protected boolean compareGuess (int guess){
        return guess == GNumber;
    }
    
}
