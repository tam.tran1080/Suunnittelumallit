package Memento;

public class Main {
    
     public static void main(String[] args) {
         
        Randomizer rand = new Randomizer();
        
        Player p1 = new Player(rand, "Player 1"); p1.start();
        Player p2 = new Player(rand, "Player 2"); p2.start();
        Player p3 = new Player(rand, "Player 3"); p3.start();
        Player p4 = new Player(rand, "Player 4"); p4.start();
        
    }
 
}
