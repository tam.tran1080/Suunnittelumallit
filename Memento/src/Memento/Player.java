package Memento;

public class Player extends Thread {
    private Memento memento; 
    private final Randomizer randomizer; 
    private final String playerName; 

    public Player(Randomizer randomizer, String name) {
        this.randomizer = randomizer;
        this.playerName = name;
    }

    
    @Override 
    public void run() { 
        randomizer.joinGame(this);
        int guess = 1; 
        while (true){ // Sort players, first who guessed right is first one
            if (randomizer.handleGuess(guess, memento)){
                System.out.println(playerName + " guessed right !");
                break;
                
            }
            guess++; 
        
        }
        
    }
    
    public void setMemento(Memento memento) {
       this. memento = memento; 
       
    }

    
    
}
