package Memento;

import java.util.Random;

public class Randomizer {
    private Random random; 
    
    public Randomizer(){
        random = new Random();
        
    }
    
    public void joinGame(Player player){
        player.setMemento(new Memento(random.nextInt(10)+1)); 
        
    }
    
    public boolean handleGuess(int guess, Memento memento){ 
        return memento.compareGuess(guess); 
    
    }
    
}
