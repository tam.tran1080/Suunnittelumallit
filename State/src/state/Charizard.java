package state;

public class Charizard implements State {
         
    @Override
    public void getState(){
        System.out.println("\n*Charizard*");
    }
    
    @Override
    public void escape(){
        System.out.println("Charizard, escape !");
    }
    
    @Override
    public void attack(){
        System.out.println("Charizard, attack ! (Dmg: 50)");
    }
    
    @Override
    public void defend(){
        System.out.println("Charizard, defend ! (Dxt: 45)");
    }
    
    @Override
    public void print(){
        getState();
        escape();
        attack();
        defend();
    }
    
}
