package state;

public class Charmander implements State {
    
    @Override
    public void getState(){
        System.out.println("*Charmader*");
    }
    
    @Override
    public void escape(){
        System.out.println("Charmander, escape !");
    }
    
    @Override
    public void attack(){
        System.out.println("Charmander, attack ! (Dmg: 20)");
    }
    
    @Override
    public void defend(){
        System.out.println("Charmander, defend ! (Dxt: 21)");
    }
    
    @Override
    public void print(){
        getState();
        escape();
        attack();
        defend();
        System.out.println("Charmander evolves into Charmeleon !");

    }
}
