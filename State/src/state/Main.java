package state;

public class Main {
    
    public static void main(String[] args){
        
        final PokemonCharacter firetype = new PokemonCharacter();
        
        firetype.print(); // Charmander
        
        firetype.setState(new Charmeleon());
        firetype.print(); 
        
        firetype.setState(new Charizard());
        firetype.print(); 
        
        
    }
    
}
