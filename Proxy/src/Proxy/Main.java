package Proxy;

import java.util.ArrayList; 
import java.util.List;

public class Main {
    
        public static void main(String[] args) {
            final Image Image1 = new ProxyImage("Image1");
            final Image Image2 = new ProxyImage("Image2");
            final Image Image3 = new ProxyImage("Image3");
            
            List<ProxyImage> list = new ArrayList<>();
            
            for (int i = 0; i<5; i++) { // Create imgs for list
                list.add(new ProxyImage("image"+(i+1)));
            
                }
            
            for (ProxyImage kuva : list) {
                System.out.println("Show "+kuva.showData()+ "'s data \n");
                
                }
             
            for (ProxyImage kuva : list) {
                kuva.displayImage();
                
                }



        }
        
}
