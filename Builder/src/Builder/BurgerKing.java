package Builder;
// Builder

import java.util.ArrayList;

public class BurgerKing implements BurgerBuilder {
    ArrayList<String> burger = new ArrayList<>();
    
    @Override
    public void addBottomBun() {
        burger.add("Bottom bun\n");
        
    }

    @Override
    public void addLettuce() {
        burger.add("Lettuce\n");
        
    }

    @Override
    public void addPickles() {
        burger.add("Pickles\n");
        
    }

    @Override
    public void addTomato() {
        burger.add("Tomato\n");
    
    }

    @Override
    public void addPatty() {
        burger.add("Patty\n");
        
    }

    @Override
    public void addSeasoning() {
        burger.add("Pepper & Salt\n");
        
    }

    @Override
    public void addCheddar() {
        burger.add("Cheddar\n");
        
    }

    @Override
    public void addTopBun() {
        burger.add("Top bun\n");   
    
    }

    @Override
    public Object getBurger() {
        return burger;
        
    }
    
}
