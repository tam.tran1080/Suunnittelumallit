package Builder;

public class Main {
    
    public static void main(String[] args){
        BurgerBuilder BkBuilder = new BurgerKing();
        BurgerBuilder McBuilder = new McDonalds();
        Director burgerStandard = new Director(BkBuilder);
        
        System.out.println("Assembling Burger King's burger");
        System.out.println("...");
        burgerStandard.buildBurger();
        System.out.println("Burger King's burger: \n" + BkBuilder.getBurger());
        System.out.println();
        
        System.out.println("Assembling McDonalds' burger");
        System.out.println("...");
        burgerStandard = new Director(McBuilder);
        burgerStandard.buildBurger();
        System.out.println("McDonalds' burger: \n" + McBuilder.getBurger());
        
    }
    
}
