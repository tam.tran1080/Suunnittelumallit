package Builder;

public class Director {
    private BurgerBuilder builder;
    
    public Director(BurgerBuilder built){
        this.builder = built;
        
    }
    
    public void buildBurger(){
        builder.addBottomBun();
        builder.addLettuce();
        builder.addPickles();
        builder.addTomato();
        builder.addPatty();
        builder.addSeasoning();
        builder.addCheddar();
        builder.addTopBun();
        
    }
    
}
