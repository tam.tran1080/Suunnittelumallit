package Builder;

public interface BurgerBuilder {
    void addBottomBun();
    void addLettuce();
    void addPickles();
    void addTomato();
    void addPatty();
    void addSeasoning();
    void addCheddar();
    void addTopBun();
    Object getBurger();
    
}
