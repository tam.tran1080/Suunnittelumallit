package Builder;
// Builder
public class McDonalds implements BurgerBuilder {
    private StringBuilder burgerBuilder = new StringBuilder();
    
    @Override
    public void addBottomBun() {
        burgerBuilder.append("Bottom bun\n");
        
    }

    @Override
    public void addLettuce() {
        burgerBuilder.append("Lettuce\n");
        
    }

    @Override
    public void addPickles() {
        burgerBuilder.append("Pickles\n");
        
    }

    @Override
    public void addTomato() {
        burgerBuilder.append("Tomato\n");
        
    }

    @Override
    public void addPatty() {
        burgerBuilder.append("Patty\n");
        
    }

    @Override
    public void addSeasoning() {
        burgerBuilder.append("Pepper, Salt & Ketchup\n");
        
    }

    @Override
    public void addCheddar() {
        burgerBuilder.append("Cheddar\n");
        
    }

    @Override
    public void addTopBun() {
        burgerBuilder.append("Top bun\n");
        
    }

    @Override
    public Object getBurger() {
        return burgerBuilder;
        
    }
    
}
