package Facade;

public class PackingMaterialStore implements Store {
    
    @Override
    public Goods getGoods() {
        PackingMaterialGoods packingGoods = new PackingMaterialGoods(); 
        return packingGoods; 
        
    }

}
