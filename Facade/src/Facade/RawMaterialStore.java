package Facade;

public class RawMaterialStore implements Store {

    @Override
    public Goods getGoods() {
        RawMaterialGoods rawGoods  = new RawMaterialGoods();
        return rawGoods;
    
    }
    
}
