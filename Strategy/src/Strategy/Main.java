package Strategy;

public class Main {
    
    public static void main(String[] args) {
        final int array_size = 10000;
        Context context;
        
        int[] a = initialArray(array_size);
        
        System.out.println("Array size: " + a.length);
        context = new Context(new QuickSort());
        long startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        long duration = System.currentTimeMillis() - startTime;
        System.out.println("Quicksort: " + (duration) + "ms");
        
        
        a = initialArray(array_size);
        context = new Context(new BubbleSort());
        startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        duration = System.currentTimeMillis() - startTime;
        System.out.println("BubbleSort: " + (duration) + "ms");
        
        a = initialArray(array_size);
        context = new Context(new SelectionSort());
        startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        duration = System.currentTimeMillis() - startTime;
        System.out.println("SelectionSort: " + (duration) + "ms");

    }

    public static int[] initialArray(int size) {
        int[] a = new int[size];
        int min = Integer.MIN_VALUE;
        int max = Integer.MAX_VALUE;
        
        for (int i = 0; i < size; i++) {
            a[i] = min + (int) (Math.random() * ((max - min) + 1));
        }
        return a;
    }
    
}
