package Strategy;

public class Context {
    
    private SortAlgorithm algorithm;
    
    public Context(SortAlgorithm algorithm){
        this.algorithm = algorithm;
        
    }
    
    public void executeStrategy(int a[]) {
        try {
            this.algorithm.sort(a);
            
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
            
        }
        
    }
    
}
