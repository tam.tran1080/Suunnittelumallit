package Strategy;

public class SelectionSort implements SortAlgorithm {
    
    @Override
    public void sort(int a[]) throws Exception {
        
        for (int i = 0; i <a.length; i++){
        int min = i;
        int j;
        
        for (j = i + 1; j < a.length; j++) {
            if (a[j] < a[min]){
                min = j;
                
                }
            
            }
        
            int T = a[min];
            a[min] = a[i];
            a[i] = T;
        }
        
    }
    
}
