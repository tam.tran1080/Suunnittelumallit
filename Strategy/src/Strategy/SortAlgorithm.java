package Strategy;

public interface SortAlgorithm {
    
    void sort(int a[]) throws Exception;
    
}
