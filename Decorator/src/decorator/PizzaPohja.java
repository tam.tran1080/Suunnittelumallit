package decorator;

public class PizzaPohja extends PizzaDecorator {     

    public PizzaPohja(PizzaInterFace topping) {         
    super(topping);

    }     
     
    @Override     
    public String getKuvaus() {         
        return super.getKuvaus() + " Pizzapohja";     

    }     

    @Override     
    public int getHinta() {         
        return super.getHinta() + 1;     
        
    }

}