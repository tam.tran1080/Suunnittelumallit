package decorator;

public class Feta extends PizzaDecorator {
    
    public Feta(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Feta";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 2;
    }
}
