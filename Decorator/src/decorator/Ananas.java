package decorator;

public class Ananas extends PizzaDecorator {
    
    public Ananas(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Ananas";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 2;
    }
}
