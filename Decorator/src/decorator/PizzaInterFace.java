package decorator;

public interface PizzaInterFace {
    
    String getKuvaus();
    int getHinta();
    
}
