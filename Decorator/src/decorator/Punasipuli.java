package decorator;

public class Punasipuli extends PizzaDecorator {
    
    public Punasipuli(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Punasipuli";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 2;
    }
}
