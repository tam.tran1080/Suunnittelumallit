package decorator;

public class Peperoni extends PizzaDecorator {
    
    public Peperoni(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Peperoni";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 4;
    }
}