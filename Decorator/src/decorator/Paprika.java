package decorator;

public class Paprika extends PizzaDecorator {
    
    public Paprika(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Paprika";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 2;
    }
}
