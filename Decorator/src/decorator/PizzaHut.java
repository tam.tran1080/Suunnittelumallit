package decorator;

import java.util.ArrayList;

public class PizzaHut {
    
    public static void main(String[] args) {         
        ArrayList<PizzaInterFace> hinnasto = new ArrayList<>();         
        hinnasto.add
                    (new JuustoReuna
                    (new  Ananas
                    (new PizzaPohja 
                    (new Peperoni 
                    (new Pizza())))));    
        
        hinnasto.add
                    (new JuustoReuna
                    (new Peperoni
                    (new PizzaPohja
                    (new Pizza()))));
        
        hinnasto.add
                    (new Paprika
                    (new Feta
                    (new Ananas
                    (new Punasipuli
                    (new PizzaPohja
                    (new Pizza()))))));
                            
            
        for (PizzaInterFace pizza : hinnasto) {             
                System.out.println(pizza.getKuvaus()+" "+pizza.getHinta()+"€");         
        
        }     
    }
}
