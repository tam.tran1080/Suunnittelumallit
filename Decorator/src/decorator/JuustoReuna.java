package decorator;

public class JuustoReuna extends PizzaDecorator {
    
    public JuustoReuna(PizzaInterFace topping){
        super(topping);
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + " Juustoreuna";
    }
    
    @Override
    public int getHinta(){
        return super.getHinta() + 2;
    }
}
