package decorator;

public class PizzaDecorator implements PizzaInterFace {
    
    protected PizzaInterFace tayte;

    public PizzaDecorator(PizzaInterFace topping) {    
        this.tayte = topping;
    
    }
    
    @Override
    public String getKuvaus(){
        return tayte.getKuvaus();
    }
    
    @Override
    public int getHinta(){
        return tayte.getHinta();
    }
    
    @Override
    public String toString(){
        return "PizzaDecorator{" +
               "description=" + tayte.getKuvaus() +
               "price=" + tayte.getHinta() + 
               '}';
    }
}
