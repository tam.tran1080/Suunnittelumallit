package Adapter;
// inherit
public class SpeedClassConverter extends MetricUnit implements SpeedUnit {

    @Override
    public double getSpeedMetric() {
        return speedMetric;
        
    }

    @Override
    public void setSpeedMetric(double speedMetric) {
        this.speedMetric = speedMetric;
        
    }

    @Override
    public double getSpeedImperial() {
        return metricToImperial(speedMetric);
        
    }

    @Override
    public void setSpeedImperial(double SpeedImperial) {
        this.speedMetric = imperialToMetric(SpeedImperial);
        
    }
    
    private double metricToImperial(double I){
        return (I / 1.60);
        
    }
    
    private double imperialToMetric(double M){
        return (M * 1.60);
        
    }
    
}
