package Adapter;
// refer
public class SpeedObjectConverter implements SpeedUnit {
    MetricUnit metricUnit;
    
    public SpeedObjectConverter(){
        metricUnit = new MetricUnit();
    
    }
    
    @Override
    public double getSpeedMetric() {
        return metricUnit.getSpeed();
    
    }

    @Override
    public void setSpeedMetric(double speedMetric) {
        metricUnit.setSpeed(speedMetric);
    
    }

    @Override
    public double getSpeedImperial() {
        return metricToImperial(metricUnit.getSpeed());
        
    }

    @Override
    public void setSpeedImperial(double speedImperial) {
        metricUnit.setSpeed(imperialToMetric(speedImperial));
    
    }
    
    private double metricToImperial(double I){
        return (I / 1.60);
        
    }
    
    private double imperialToMetric(double M){
        return (M * 1.60);
        
    }
}
