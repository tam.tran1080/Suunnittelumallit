package Adapter;
// main
public class Adapter {
    
    public static void main(String[] args){
        
        // Class adapter in action
        System.out.println("Class adapter");
        SpeedUnit speedU = new SpeedClassConverter();
        showConvertedUnits(speedU);
        
        // Object adapter in action
        System.out.println();
        System.out.println("Object adapter");
        speedU = new SpeedObjectConverter();
        showConvertedUnits(speedU);
        
    }
    
    public static void showConvertedUnits(SpeedUnit speedU){
        speedU.setSpeedMetric(1);
        System.out.println("Speed in km/h: " + speedU.getSpeedMetric());
        System.out.println("Speed in m/h: " + speedU.getSpeedImperial());
        
        speedU.setSpeedImperial(100);
        System.out.println("Speed in km/h: " + speedU.getSpeedMetric());
        System.out.println("Speed in m/h: " + speedU.getSpeedImperial());
<<<<<<< HEAD
        System.out.println("");
=======
        
>>>>>>> 08805ba1bb7658f07816ae2b4d2b8f37b9b40cb1
    }
    
}
