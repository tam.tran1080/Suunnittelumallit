package Adapter;

public class MetricUnit {
    double speedMetric;
    
    public double getSpeed(){
        return speedMetric;
    }
    
    public void setSpeed(double speedMetric){
        this.speedMetric = speedMetric;
    }
    
}
