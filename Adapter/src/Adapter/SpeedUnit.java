package Adapter;

public interface SpeedUnit {
    public double getSpeedMetric();
    public void setSpeedMetric(double speedMetric);
    
    public double getSpeedImperial();
    public void setSpeedImperial(double speedImperial);
    
}
