package Visitor;

public class PokemonCharacter implements State {
    
    private State state;
    
    public PokemonCharacter(){
        setState(new Charmander());
    }
    
    public void setState(final State changedState) {         
        state = changedState;     
    } 
    
    @Override
    public void escape(){
        state.escape();
    }
    
    @Override
    public void attack(){
        state.attack();
    }
    
    @Override
    public void defend(){
        state.defend();
    }
    
    @Override
    public void getState(){
        state.getState();
    }
    
    @Override
    public void accept(Visitor visitor) {
        state.accept(visitor);
    }

    
    @Override
    public void print(){
        state.print();
    }

}
