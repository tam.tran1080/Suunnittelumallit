package Visitor;

public class Charmeleon implements State {
    
    @Override
    public void getState(){
        System.out.println("\n*Charmeleon*");
    }
    
    @Override
    public void escape(){
        System.out.println("Charmeleon, escape !");
    }
    
    @Override
    public void attack(){
        System.out.println("Charmeleon, attack ! (Dmg: 34)");
    }
    
    @Override
    public void defend(){
        System.out.println("Charmeleon, defend ! (Dxt: 32)");
    }
    
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    
    @Override
    public void print(){
        getState();
        escape();
        attack();
        defend();
        System.out.println("Charmeleon evolves into Charizard !");
        
    }
}
