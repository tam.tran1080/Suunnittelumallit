package Visitor;

public interface State {
    
    public void getState();
    public void escape();
    public void attack();
    public void defend();
    public void print();
    public void accept(Visitor visitor);
    
}
    
    
    

