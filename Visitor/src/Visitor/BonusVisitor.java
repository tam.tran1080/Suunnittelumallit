package Visitor;

public class BonusVisitor implements Visitor {

    @Override
    public void visit(Charmander charmander) {
        System.out.println("+3 bonus points earned");
    }

    @Override
    public void visit(Charmeleon charmeleon) {
        System.out.println("+5 bonus points earned");
    }

    @Override
    public void visit(Charizard charizard) {
        System.out.println("+7 bonus points earned");
    }
    
}
