package Visitor;

public class Main {
    
    public static void main(String[] args){
        
        final PokemonCharacter firetype = new PokemonCharacter();
        final BonusVisitor visitor = new BonusVisitor();
        
        firetype.print(); // Charmander
        firetype.accept(visitor);
        
        firetype.setState(new Charmeleon());
        firetype.print(); 
        firetype.accept(visitor);
        
        firetype.setState(new Charizard());
        firetype.print(); 
        firetype.accept(visitor);
 
    }
    
}
