package Command;

public class Main {
    public static void main(String[] args){
    Screen screen           = new Screen();
    Command switchUp        = new UpButton(screen);
    Command switchDown      = new DownButton(screen);
    WallButton buttonUp     = new WallButton(switchUp);
    WallButton buttonDown   = new WallButton(switchDown);
    
    buttonUp.push();
    buttonDown.push();
    
    }
    
}
