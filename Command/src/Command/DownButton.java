package Command;

public class DownButton implements Command {
    private Screen scrn;
    
    public DownButton(Screen scrn){
        this.scrn = scrn;
        
    }
    
    @Override
    public void execute() {
        scrn.down();
        
    }
    
}
