package Command;

public class UpButton implements Command {
    private Screen scrn;

    public UpButton(Screen scrn) {
        this.scrn = scrn;
    }
    
    @Override
    public void execute() {
        scrn.up();
        
    }
    
}
