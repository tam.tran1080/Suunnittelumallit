package Prototype;

public class SecondHand extends Hands implements Cloneable {
    private int secs;
    
    public void tick(){
        if (secs == 59){
            secs = 0;
            
        }
        else {
            secs++;
            
        }
        
    }

    public int getTime() {
        return secs;
        
    }

    @Override
    public void setTime(int time) {
        if (time >= 0 && time <= 59){
            secs = time;
            
        }
        else {
            System.out.println("Set seconds between 0 - 59");
            
        }
        
    }
      
}
