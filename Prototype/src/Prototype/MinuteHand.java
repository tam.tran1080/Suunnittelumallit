package Prototype;

public class MinuteHand extends Hands implements Cloneable {
    private int mins;
    
    public void tick(){
        if (mins == 59){
            mins = 0;
            
        }
        else {
            mins++;
            
        }
        
    }

    public int getTime() {
        return mins;
        
    }

    @Override
    public void setTime(int time) {
        if (time >= 0 && time <= 59){
            mins = time;
            
        }
        else {
            System.out.println("Set mins between 0 - 59");
            
        }
    }
      
}
