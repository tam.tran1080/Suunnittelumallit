package Prototype;

public abstract class Hands {
    public abstract void tick();
    public abstract int getTime();
    public abstract void setTime(int time);
    
    public Object clone(){
        try {
            return super.clone();
            
        } 
        
        catch(CloneNotSupportedException e){
            e.printStackTrace();
        
        }
        return null;
        
    }    
}
