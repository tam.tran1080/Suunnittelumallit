package Prototype;

public class Clock implements Cloneable {
    Hands hHand;
    Hands mHand;
    Hands sHand;
    
    public Clock(){
        this.hHand = new HourHand();
        this.mHand = new MinuteHand();
        this.sHand = new SecondHand();
    
    }

    public Clock clone(){
        Clock cSuper = null;
        
        try {
            cSuper = (Clock) super.clone();
            cSuper.hHand = (Hands) hHand.clone();
            cSuper.mHand = (Hands) mHand.clone();
            cSuper.sHand = (Hands) sHand.clone();
            
        }
        catch(CloneNotSupportedException e){
            // Empty
        }
        return cSuper;
    
    }
    
    public void tick(){
        sHand.tick();
        
        if(sHand.getTime() == 0){
        mHand.tick();
        }
        
        if (mHand.getTime() == 0){
            hHand.tick();
        }
        
    }
    
}


