package Prototype;

public class HourHand extends Hands implements Cloneable {
    private int hours;
    
    public void tick(){
        if (hours == 23){
            hours = 0;
            
        }
        else {
            hours++;
            
        }
        
    }

    public int getTime() {
        return hours;
        
    }

    @Override
    public void setTime(int time) {
        if (time >= 0 && time <= 23){
            hours = time;
            
        }
        else {
            System.out.println("Set hours between 0 - 23");
            
        }
    }
}
