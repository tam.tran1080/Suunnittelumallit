package Prototype;

public class Main {
    
    public static void main(String[] args){
        Clock clock = new Clock();
        Clock cloneClock = new Clock();
        
        int hour = 9;
        int min  = 29;
        int sec  = 59;
        
        int chour = 15;
        int cmin  = 29;
        int csec  = 39;

        clock.hHand.setTime(hour);
        clock.mHand.setTime(min);
        clock.sHand.setTime(sec);
        
        cloneClock.hHand.setTime(chour);
        cloneClock.mHand.setTime(cmin);
        cloneClock.sHand.setTime(csec);
        
        System.out.println("Original time below");
        System.out.println("Time in Finland: " + clock.hHand.getTime() + "." + clock.mHand.getTime() + "." + clock.mHand.getTime());
        System.out.println("Time in South-Korea: " + cloneClock.hHand.getTime() + "." + cloneClock.mHand.getTime() + "." + cloneClock.mHand.getTime());
        System.out.println();
        
        System.out.println("New ticking time below");

        for(int i = 0; i < 24; i++){
            cloneClock.tick();
            clock.tick();
            System.out.println("Time in Finland: " + clock.hHand.getTime() + "." + clock.mHand.getTime() + "." + clock.mHand.getTime());
            System.out.println("Time in South-Korea: " + cloneClock.hHand.getTime() + "." + cloneClock.mHand.getTime() + "." + cloneClock.mHand.getTime());
            System.out.println();
        
        }

        
    }
    
}
