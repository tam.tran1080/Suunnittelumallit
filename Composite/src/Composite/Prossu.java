
package Composite;

public class Prossu implements Palikka {

    int hinta;
    
    public Prossu(int hinta){
        this.hinta = hinta;
    }
    
    @Override
    public int getHinta() {
        return hinta;
    }
     
}