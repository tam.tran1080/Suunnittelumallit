package Composite;

public class Ram implements Palikka {

    int hinta;
    
    public Ram(int hinta){
        this.hinta = hinta;
    }
    
    @Override
    public int getHinta() {
        return hinta;
    }
     
}
