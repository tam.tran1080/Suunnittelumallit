package Composite;

public interface AbstractFactory {
    
    public Palikka CreateKoppa();
    public Palikka CreateRam();
    public Palikka CreateEmo();
    public Palikka CreateProssu();
    public Palikka CreateVerkko();
    public Palikka CreateOhjain();
    
}
