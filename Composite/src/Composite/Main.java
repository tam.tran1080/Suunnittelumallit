package Composite;

public class Main {
    
    public static void main(String[] args){
        
        // Kopan hinta
        OsaPalikka kaikkiosat = new Koppa(100);
        
        // Emon hinta ja siihen kiinnittyvät komponentit
        OsaPalikka sisusosat = new Emolevy(100);
        sisusosat.addOsaPalikka(new Prossu(250));
        sisusosat.addOsaPalikka(new Ohjain(250));
        sisusosat.addOsaPalikka(new Ram(250));
        sisusosat.addOsaPalikka(new Verkko(250));
        
        kaikkiosat.addOsaPalikka(sisusosat);
        System.out.println("Yhteensä: " + kaikkiosat.getHinta());

    }
}
