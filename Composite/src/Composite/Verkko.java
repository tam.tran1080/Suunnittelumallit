package Composite;
// Verkkokortti
public class Verkko implements Palikka {

    int hinta;
    
    public Verkko(int hinta){
        this.hinta = hinta;
    }
    
    @Override
    public int getHinta() {
        return hinta;
    }
     
}