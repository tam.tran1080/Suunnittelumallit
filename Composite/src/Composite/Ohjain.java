package Composite;
// Näytönohjain
public class Ohjain implements Palikka {

    int hinta;
    
    public Ohjain(int hinta){
        this.hinta = hinta;
    }
    
    @Override
    public int getHinta() {
        return hinta;
    }
     
}
