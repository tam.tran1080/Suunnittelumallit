package Composite;

import java.util.ArrayList; 
import java.util.List;
// Kopan sisälle tulee kaikki muut osat, mutta muista kopan hinta myös
public class Koppa implements OsaPalikka {

    int hinta;
    List<Palikka> kopanOsat = new ArrayList<>();
    
    public Koppa(int hinta){
        this.hinta = hinta;
    }
    
    public void addOsaPalikka(Palikka p){
        if (!kopanOsat.contains(p)){
            kopanOsat.add(p);         
        }
    }

    @Override
    public int getHinta(){
        int sumHinta = this.hinta;
        for (Palikka palikka : kopanOsat){
            sumHinta += palikka.getHinta();
        }
        return sumHinta;
    }
    
}
