package Composite;
// Osapalikka on kooste palikka, johon kasataan muut osat
public interface OsaPalikka extends Palikka {
    
    public void addOsaPalikka(Palikka p);
    
}
