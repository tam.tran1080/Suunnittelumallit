package Composite;

import java.util.ArrayList;
import java.util.List;

// Emolevy on kooste palikka, johon koota muut osat
public class Emolevy implements OsaPalikka {
    
    int hinta;
    List<Palikka> emoPalikat = new ArrayList<>();
    
    public Emolevy(int hinta){
        this.hinta = hinta;
    }

    @Override
    public void addOsaPalikka(Palikka p) {
    
        if (!emoPalikat.contains(p)){
            emoPalikat.add(p);
        }
    }

    @Override
    public int getHinta() {
        
        int sumHinta = this.hinta;
        for (Palikka palikka : emoPalikat){
            sumHinta += palikka.getHinta();
        }
        return sumHinta;
    }
    
}

